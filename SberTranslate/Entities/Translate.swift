//
//  Translate.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 08.12.2019.
//  Copyright © 2019 KZolotarev. All rights reserved.
//

import CoreData

@objc(Translate)
class Translate: NSManagedObject, Codable {
	
	enum CodingKeys: String, CodingKey {
		case code = "code"
		case lang = "lang"
		case text = "text"
		case baseText = "baseText"
		case date = "date"
		case direction = "direction"
	}
	
	@NSManaged var code: Int
	@NSManaged var lang: String
	@NSManaged var baseText: String
	@NSManaged var text: [String]
	@NSManaged var date: Date
	@NSManaged var direction: String
	
	required convenience init(from decoder: Decoder) throws {
		guard let contextUserInfoKey = CodingUserInfoKey.context,
			let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
			let entity = NSEntityDescription.entity(forEntityName: "Translate", in: managedObjectContext)
			else {
				fatalError("Failed to decode Translate")
		}
		
		self.init(entity: entity, insertInto: managedObjectContext)
		let values = try decoder.container(keyedBy: CodingKeys.self)
		do {
			code = try values.decode(Int.self, forKey: .code)
			lang = try values.decode(String.self, forKey: .lang)
			text = try values.decode([String].self, forKey: .text)
		} catch {
			print("error Translate")
		}
	}
	
	func encode(to encoder: Encoder) throws {
		var values = encoder.container(keyedBy: CodingKeys.self)
		do {
			try values.encode(code, forKey: .code)
			try values.encode(lang, forKey: .lang)
			try values.encode(text, forKey: .text)
			try values.encode(baseText, forKey: .baseText)
			try values.encode(date, forKey: .date)
			try values.encode(direction, forKey: .direction)
		} catch {
			print("error Translate")
		}
	}
}
