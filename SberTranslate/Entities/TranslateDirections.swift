//
//  TranslateDirections.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 13.12.2019.
//  Copyright © 2019 KZolotarev. All rights reserved.
//

import CoreData

@objc(TranslateDirections)
class TranslateDirections: NSManagedObject, Codable {
	
	enum CodingKeys: String, CodingKey {
		case dirs = "dirs"
		case langs = "langs"
	}
	
    @NSManaged var dirs: [String]
    @NSManaged var langs: [String:String]
	
	required convenience init(from decoder: Decoder) throws {
		guard let contextUserInfoKey = CodingUserInfoKey.context,
			let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
			let entity = NSEntityDescription.entity(forEntityName: "TranslateDirections", in: managedObjectContext)
			else {
				fatalError("Failed to decode TranslateDirections")
		}
		
		self.init(entity: entity, insertInto: managedObjectContext)
		let values = try decoder.container(keyedBy: CodingKeys.self)
		do {
			langs = try values.decode([String:String].self, forKey: .langs)
			dirs = try values.decode([String].self, forKey: .dirs)
		} catch {
			print("error TranslateDirections")
		}
	}
	
	func encode(to encoder: Encoder) throws {
		var values = encoder.container(keyedBy: CodingKeys.self)
		do {
			try values.encode(langs, forKey: .langs)
			try values.encode(dirs, forKey: .dirs)
		} catch {
			print("error TranslateDirections")
		}
	}
	
	func posibleDirections(fromLanguage: String = TranslateNetworkService.fromLanguage) -> [String] {
		var posible = [String]()
		for direction: String in dirs {
			let components = direction.split(separator: "-")
			let sub = String(components[0])
			if sub == fromLanguage {
				posible.append(String(components[1]))
			}
		}
		return posible
	}
	
	func allDirections() -> [String] {
		var allKeys = [String]()
		for key in langs.keys {
			allKeys.append(key)
		}
		return allKeys
	}
	
	func nativeLanguageName(code:String) -> String {
		return (code == "" ? "Chose Language".localized() : langs[code] ?? "Wrong Language Code".localized())
	}
}
