//
//  NSUserDefaults.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 21.12.2019.
//  Copyright © 2019 KZolotarev. All rights reserved.
//

import UIKit

extension UserDefaults {
	@objc dynamic var currentTranslation: String {
		
        get {
			return string(forKey: "currentTranslation") ?? "-"
        }

        set {
			
			let components = newValue.split(separator: "-")
			if components.count == 2 {
				TranslateNetworkService.fromLanguage = String(components[0])
				TranslateNetworkService.toLanguage = String(components[1])
			} else if components.count == 1 {
				TranslateNetworkService.fromLanguage = String(components[0])
				TranslateNetworkService.toLanguage = ""
			} else {
				TranslateNetworkService.fromLanguage = ""
				TranslateNetworkService.toLanguage = ""
			}
            set(newValue, forKey: "currentTranslation")
        }
    }
}
