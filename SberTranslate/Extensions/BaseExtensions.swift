//
//  BaseExtensions.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 08.12.2019.
//  Copyright © 2019 KZolotarev. All rights reserved.
//

import Foundation

extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            return escapedKey + "=" + escapedValue
            }
            .joined(separator: "&")
    }
}

extension String {
	func localized() -> String {
		return NSLocalizedString(self, comment: "")
	}
}
