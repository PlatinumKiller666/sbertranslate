//
//  TranslateCoreDataService.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 08.12.2019.
//  Copyright © 2019 KZolotarev. All rights reserved.
//

import UIKit
import CoreData

class TranslateCoreDataService: NSObject {

	public static func getDirections() -> TranslateDirections {

		let context = AppDelegate.sharedAppDelegate.persistentContainer.viewContext
		
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TranslateDirections")
		do {
			if let result = try context.fetch(request) as? [TranslateDirections] {
				return result.count > 0 ? result[0] : TranslateDirections()
			}
		} catch {
		}
		return TranslateDirections()
	}
	
	public static func getTranslations(searchText: String) -> [Translate] {
		
		
		let context = AppDelegate.sharedAppDelegate.persistentContainer.viewContext

		let request = NSFetchRequest<Translate>(entityName: "Translate")
		let sort = NSSortDescriptor(key: "date", ascending: false)
		request.sortDescriptors = [sort]

		if !searchText.isEmpty {
			
//			Old problem https://forums.developer.apple.com/thread/99490
//			request.predicate = NSPredicate(format: "(text contains[c] %@) OR (baseText contains[c] %@)", searchText, searchText)
//			request.predicate = NSPredicate(format: "text CONTAINS %@", searchText)
		}
		
		do {
			var result = try context.fetch(request)


			if !searchText.isEmpty {

				let searchString = searchText.lowercased()
				result = result.filter { (translate) -> Bool in
				let lText = translate.text[0].lowercased()
				let lBaseText = translate.baseText.lowercased()
					return lText.contains(searchString) || lBaseText.contains(searchString)
				}
			}
			
			return result
		} catch {
		}
		return [Translate]()
	}
	
	
}
