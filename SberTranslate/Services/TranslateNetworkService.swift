//
//  TranslateNetworkService.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 08.12.2019.
//  Copyright © 2019 KZolotarev. All rights reserved.
//

import UIKit

//class URLEr

typealias DirectionsComplection = (TranslateDirections?, Error?) -> Void
typealias TranslateComplection = (Translate?, Error?) -> Void

protocol Api {
	func getLangs(langCode:String, complection: @escaping DirectionsComplection)
	func translate(text: String, complection: @escaping TranslateComplection)
	func translateDirect(text:String, direction:String, complection: @escaping TranslateComplection)
}

extension Api {
	
}

class TranslateNetworkService: Api {
	
	enum selectionLanguage{
		case from
		case to
	}
	
	private let baseUrl = "https://translate.yandex.net/api/v1.5/tr.json/"
	private let langPath = "getLangs"
	private let translatePath = "translate"
	
	static let shared = TranslateNetworkService()
	static var fromLanguage = ""
	static var toLanguage = ""
	static var selectChange = selectionLanguage.from
	
	lazy var sessionManager = URLSession(configuration: URLSessionConfiguration.default)
	
	init() {
		
		let components = UserDefaults.standard.currentTranslation.split(separator: "-")
		if components.count == 2 {
			TranslateNetworkService.fromLanguage = String(components[0])
			TranslateNetworkService.toLanguage = String(components[1])
		}
	}
	
	func getLangs(langCode: String, complection: @escaping DirectionsComplection) {
		let parameters: [String: Any] = [
			"ui"    : langCode,
			"key"   : ServiceConstants.tranlateAPIKey
		]
		guard let urlBase = URL(string: baseUrl) else {
			let error = URLError(.badURL)
			complection(nil, error)
			return
		}
		
		let url = urlBase.appendingPathComponent(langPath)
		
		var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 10)
		request.httpMethod = "POST"
		
		request.httpBody = parameters.percentEscaped().data(using: .utf8)
		
		let task = sessionManager.dataTask(with: request) { data, response, error in
			guard let data = data,
				let response = response as? HTTPURLResponse,
				error == nil else {                                              // check for fundamental networking error
					print("error", error ?? "Unknown error")
					
					let error = URLError(.unknown)
					complection(nil, error)
					return
			}
			
			guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
				let error = URLError(URLError.Code(rawValue: response.statusCode))
				complection(nil, error)
				return
			}
			
			DispatchQueue.main.async {
				let decoder = JSONDecoder()
				
				let app = AppDelegate.sharedAppDelegate
				let coordinator = app.persistentContainer.viewContext
				decoder.userInfo[CodingUserInfoKey.context!] = coordinator
				app.clearDirections()
				
				let directions: TranslateDirections = try! decoder.decode(TranslateDirections.self, from: data)
				
				app.saveContext()
				
				complection(directions, nil)
			}
		}
		
		task.resume()
	}
	
	func translate(text: String, complection: @escaping TranslateComplection) {
		if TranslateNetworkService.fromLanguage.isEmpty || TranslateNetworkService.toLanguage.isEmpty {
			return
		}
		translateDirect(text: text, direction: "\(TranslateNetworkService.fromLanguage)-\(TranslateNetworkService.toLanguage)", complection: complection)
	}
	
	func translateDirect(text: String, direction: String, complection: @escaping TranslateComplection) {
		let parameters: [String: Any] = [
			"text"  : text,//.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
			"key"   : ServiceConstants.tranlateAPIKey,
			"lang"  : direction,
			"format": "plain"
		]
		
		guard let urlBase = URL(string: baseUrl) else {
			let error = URLError(.badURL)
			complection(nil, error)
			return
		}
		
		let url = urlBase.appendingPathComponent(translatePath)
		
		var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 10)
		request.httpMethod = "POST"
		
		request.httpBody = parameters.percentEscaped().data(using: .utf8)
		
		let task = sessionManager.dataTask(with: request) { data, response, error in
			guard let data = data,
				let response = response as? HTTPURLResponse,
				error == nil else {
					let error = URLError(.unknown)
					complection(nil, error)
					return
			}
			
			guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
				let error = URLError(URLError.Code(rawValue: response.statusCode))
				complection(nil, error)
				return
			}
			
			DispatchQueue.main.async {
				let decoder = JSONDecoder()
				let app = AppDelegate.sharedAppDelegate
				let coordinator = app.persistentContainer.viewContext
				decoder.userInfo[CodingUserInfoKey.context!] = coordinator
				
				let translate: Translate = try! decoder.decode(Translate.self, from: data)
				translate.baseText = text
				translate.date = Date()
				translate.direction = direction
				app.saveContext()
				
				complection(translate, nil)
			}
		}
		
		task.resume()
	}
	
}
