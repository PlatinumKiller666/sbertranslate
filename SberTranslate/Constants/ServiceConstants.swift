//
//  ServiceConstants.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 08.12.2019.
//  Copyright © 2019 KZolotarev. All rights reserved.
//

import UIKit

class ServiceConstants: NSObject {
    static let tranlateAPIKey = "trnsl.1.1.20191208T115601Z.b7dd3a462ec279eb.39c1d200998b17a3740e72fa3cca08629506f1a0"
}

extension CodingUserInfoKey {
	static let context = CodingUserInfoKey(rawValue: "context")
}
