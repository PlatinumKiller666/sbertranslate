//
//  TranslationDirectionView.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 08.12.2019.
//  Copyright © 2019 KZolotarev. All rights reserved.
//

import UIKit

class TranslationDirectionView: UIView {
	
	var holderController: BaseWireframe?
	var observer: NSKeyValueObservation?
	
	lazy var fromButton : UIButton = {
		let button = UIButton(type: .system)
		self.addSubview(button)
		
		button.translatesAutoresizingMaskIntoConstraints = false
		
		button.setTitle("from language".localized(), for: .normal)
		button.tintColor = .black
		button.addTarget(self, action: #selector(TranslationDirectionView.popChosseView(sender:)), for: .touchUpInside)
		return button
	}()
	
	lazy var toButton : UIButton = {
		let button = UIButton(type: .system)
		self.addSubview(button)
		
		button.translatesAutoresizingMaskIntoConstraints = false
		
		button.setTitle("to language".localized(), for: .normal)
		button.tintColor = .black
		button.addTarget(self, action: #selector(TranslationDirectionView.popChosseView(sender:)), for: .touchUpInside)
		return button
	}()
	
	lazy var swapButton : UIButton = {
		let button = UIButton(type: .system)
		self.addSubview(button)
		
		button.translatesAutoresizingMaskIntoConstraints = false
		button.imageView?.contentMode = .scaleAspectFit
		button.setImage(UIImage(named: "directions"), for: .normal)
		
		button.tintColor = .black
		button.heightAnchor.constraint(equalToConstant: 44).isActive = true
		button.widthAnchor.constraint(equalToConstant: 44).isActive = true
		
		button.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
		button.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
		
		button.addTarget(self, action: #selector(TranslationDirectionView.swapLanguages(sender:)), for: .touchUpInside)
		return button
	}()
	
	@IBAction func popChosseView(sender: UIButton!) {
		switch sender {
		case fromButton:
			TranslateNetworkService.selectChange = .from
			holderController?.viewController.presentWireframeInNavigation(LanguageSelectControllerWireframe())
			break
		case toButton:
			if TranslateNetworkService.fromLanguage.isEmpty {
				holderController?.showErrorAlert(with: "Chose from language first")
				return
			}
			
			TranslateNetworkService.selectChange = .to
			holderController?.viewController.presentWireframeInNavigation(LanguageSelectControllerWireframe())
			break
		default:
			break
		}
	}
	
	@IBAction func swapLanguages(sender: UIButton!) {
		if TranslateNetworkService.toLanguage.isEmpty || TranslateNetworkService.fromLanguage.isEmpty {
			holderController?.showErrorAlert(with: "Chose languages for translations")
			return
		}
		let buferLanguage = TranslateNetworkService.fromLanguage
		TranslateNetworkService.fromLanguage = TranslateNetworkService.toLanguage
		TranslateNetworkService.toLanguage = buferLanguage
		
		UserDefaults.standard.currentTranslation = "\(TranslateNetworkService.fromLanguage)-\(TranslateNetworkService.toLanguage)"
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		observer = UserDefaults.standard.observe(\.currentTranslation, options: [.old, .initial, .new], changeHandler: { [unowned self] (defaults, change) in
			self.updateView()
		})
		setupView()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setupView()
	}
	
	private func setupView() {
		backgroundColor = .clear
		
		self.translatesAutoresizingMaskIntoConstraints = false
		
		//Top Anchors
		fromButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
		toButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
		
		//Bottom Anchors
		fromButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
		toButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
		
		//Left Anchors
		fromButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
		toButton.leftAnchor.constraint(equalTo: swapButton.rightAnchor, constant: 8).isActive = true
		
		//Rigt Anchors
		fromButton.rightAnchor.constraint(equalTo: swapButton.leftAnchor, constant: -8).isActive = true
		toButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
		
		toButton.widthAnchor.constraint(equalTo: fromButton.widthAnchor, multiplier: 1.0)
		
		self.translatesAutoresizingMaskIntoConstraints = false
		
//		updateView()
	}
	
	func updateView() {
		let directions = TranslateCoreDataService.getDirections()
		
		fromButton.setTitle(directions.nativeLanguageName(code: TranslateNetworkService.fromLanguage)
			, for: .normal)
		toButton.setTitle(directions.nativeLanguageName(code: TranslateNetworkService.toLanguage)
				, for: .normal)
	}
	
	
	deinit {
		observer?.invalidate()
	}
}
