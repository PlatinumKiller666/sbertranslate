//
//  DictionaryControllerInteractor.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 17.12.2019.
//  Copyright (c) 2019 KZolotarev. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import Foundation

final class DictionaryControllerInteractor {
}

// MARK: - Extensions -

extension DictionaryControllerInteractor: DictionaryControllerInteractorInterface {
	func fetchDictionary(searchText: String, complection: @escaping   (([Translate]) -> Void)) {
		
		DispatchQueue.main.async {

			let result = TranslateCoreDataService.getTranslations(searchText: searchText)
			complection(result)
		}
	}
}
