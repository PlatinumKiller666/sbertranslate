//
//  DictionaryCell.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 17.12.2019.
//  Copyright © 2019 KZolotarev. All rights reserved.
//

import UIKit

class DictionaryCell: UITableViewCell {
	static let id = "DictionaryCell"
	var translate : Translate? {
		didSet {
			baseTextTranslationLabel.text = translate?.baseText
			textTranslationLabel.text = translate?.text[0]
		}
	}
	
	lazy var baseTextTranslationLabel : UILabel =  {
		let label = UILabel()
		label.numberOfLines = 0
		self.contentView.addSubview(label)
		
		label.translatesAutoresizingMaskIntoConstraints = false
		
		label.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
		label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8).isActive = true
		label.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8).isActive = true
		
		label.contentMode = .topLeft
		
		
		return label
	} ()
	
	lazy var textTranslationLabel : UILabel = {
		let label = UILabel()
		label.numberOfLines = 0
		self.contentView.addSubview(label)
		
		label.translatesAutoresizingMaskIntoConstraints = false
		
		label.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
		label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8).isActive = true
		label.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -8).isActive = true
		label.leftAnchor.constraint(equalTo: baseTextTranslationLabel.rightAnchor, constant: 8).isActive = true
		label.widthAnchor.constraint(equalTo: baseTextTranslationLabel.widthAnchor, constant: 0).isActive = true
		
		label.contentMode = .topLeft
		
		return label
	} ()
	

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
	
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
