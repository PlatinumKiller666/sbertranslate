//
//  DictionaryControllerWireframe.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 17.12.2019.
//  Copyright (c) 2019 KZolotarev. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class DictionaryControllerWireframe: BaseWireframe {

    // MARK: - Private properties -

    // MARK: - Module setup -

    init() {
        let moduleViewController = DictionaryControllerViewController()
        super.init(viewController: moduleViewController)

        let interactor = DictionaryControllerInteractor()
        let presenter = DictionaryControllerPresenter(view: moduleViewController, interactor: interactor, wireframe: self)
        moduleViewController.presenter = presenter
    }

}

// MARK: - Extensions -

extension DictionaryControllerWireframe: DictionaryControllerWireframeInterface {
	func showTranslate(translate:Translate) {
		UserDefaults.standard.currentTranslation = translate.direction
		TabBarControllerWireframe.shared.showTranslate(translate: translate)
	}
}
