//
//  TabBarControllerPresenter.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 17.12.2019.
//  Copyright (c) 2019 KZolotarev. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class TabBarControllerPresenter {

    // MARK: - Private properties -

    private unowned let view: TabBarControllerViewInterface
    private let interactor: TabBarControllerInteractorInterface
    private let wireframe: TabBarControllerWireframeInterface

    // MARK: - Lifecycle -

    init(view: TabBarControllerViewInterface, interactor: TabBarControllerInteractorInterface, wireframe: TabBarControllerWireframeInterface) {
        self.view = view
        self.interactor = interactor
        self.wireframe = wireframe
    }
}

// MARK: - Extensions -

extension TabBarControllerPresenter: TabBarControllerPresenterInterface {
}
