//
//  LanguageSelectorNavigation.swift
//  SberTranslate
//
//  Created by Kirill Zolotarev on 13.12.2019.
//  Copyright © 2019 KZolotarev. All rights reserved.
//

import UIKit

class LanguageSelectorNavigation: UINavigationController {
	
	let titleView = TranslationDirectionView()

    override func viewDidLoad() {
        super.viewDidLoad()

		
        // Do any additional setup after loading the view.
    }
    
	override func viewDidAppear(_ animated: Bool) {
		titleView.updateView()
		self.navigationItem.titleView = titleView
		self.navigationBar.topItem?.titleView = titleView
	}
	
	func setWireframe(wireFrame: BaseWireframe){
		titleView.holderController = wireFrame
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
